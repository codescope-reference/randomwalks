#include "raylib.h"
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <stdbool.h>
#include <time.h>


size_t WWIDTH;
size_t WHEIGHT;


typedef struct Point {
    int x;
    int y;
} Point;

#define HTABLETYPE Point
#define HTABLENAME PointSet
#define Point_EQ(pt1, pt2) ((pt1).x == (pt2).x && (pt1).y == (pt2).y)
#define HASHTABLE_HASHSET_IMPL
#include "hashset.h"


float random_float(float min, float max) {
    return (float)rand() / RAND_MAX * (max - min) + min;
}

int rand_int(int min, int max) {
    return rand() % (max - min + 1) + min;
}

#define NUMBER_OF_STEPS 20000
PointSet *visited_points = NULL;

Point *random_walk(int *count) {
    Point *pts = malloc(sizeof(Point) * NUMBER_OF_STEPS);


    int startx = rand_int(-WWIDTH/2, WWIDTH/2);
    int starty = rand_int(-WHEIGHT/2, WHEIGHT/2);
    pts[0] = (Point) { startx, starty };
    int max_tries = 100;
    int tries = 0;
    while (tries < max_tries && elem_PointSet(visited_points, pts[0])) {
        startx = rand_int(0, WWIDTH);
        starty = rand_int(0, WHEIGHT);
        pts[0] = (Point) { startx, starty };
        tries++;
    }
    if (elem_PointSet(visited_points, pts[0])) {
        *count = 0;
        return pts;
    }

    insert_PointSet(visited_points, pts[0]);
    int local_count = 1;
    for (size_t i = 1; i < NUMBER_OF_STEPS; i++) {
        Point p = pts[i-1];
        int r = GetRandomValue(0, 5);
        bool visited = false;
        int n_tried = 0;
start_of_switch:
        switch (r) {
            case 0:
                // Go right
                n_tried += 1;
                p.x += 1;
                visited = elem_PointSet(visited_points, p);
                if (!visited) {
                    insert_PointSet(visited_points, p);
                    break;
                }
                p.x -= 1;
            case 1:
                n_tried += 1;
                // Go left
                p.x -= 1;
                visited = elem_PointSet(visited_points, p);
                if (!visited) {
                    insert_PointSet(visited_points, p);
                    break;
                }
                p.x += 1;
            case 2:
            case 4:
            case 5:
                n_tried += 1;
                // Go up
                p.y += 1;
                visited = elem_PointSet(visited_points, p);
                if (!visited) {
                    insert_PointSet(visited_points, p);
                    break;
                }
                p.y -= 1;
            case 3:
                n_tried += 1;
                // Go down
                p.y -= 1;
                visited = elem_PointSet(visited_points, p);
                if (!visited) {
                    insert_PointSet(visited_points, p);
                    break;
                }
                p.y += 1;
            default:
                if (n_tried < 4) {
                    r = 0;
                    goto start_of_switch;
                } else {
                    // for (int __IDX = 0; __IDX < visited_points->nbuckets; __IDX++) { 
                        // if (visited_points->buckets[__IDX] == ((void *)0)) { 
                            // continue;
                        // } else { 
                            // for (__PointSetHTABLELIST * node = visited_points->buckets[__IDX]; node != ((void *)0); node = node ->next) { 
                                // int current_x = node->value.x;
                                // int current_y = node->value.y;
                                // if (abs(current_x - p.x) == 1 && current_y == p.y) {
                                    // printf("(%d, %d)\n", node->value.x, node->value.y); 
                                // }
                                // if (abs(current_y - p.y) == 1 && current_x == p.x) {
                                    // printf("(%d, %d)\n", node->value.x, node->value.y); 
                                // }
                            // } 
                        // } 
                    // }
                    // printf("Current point: (%d, %d)\n", p.x, p.y);
                    goto end;
                }
        }
        pts[i] = p;
        local_count++;
    }
end:
    *count = local_count;
    Point *newpts = malloc(sizeof(Point) * local_count);
    for (int i = 0; i < local_count; i++) {
        newpts[i] = pts[i];
    }
    free(pts);

    return newpts;
}


#define NUMBER_OF_RUNS 5000
#define NUMBER_OF_COLORS 5
const int alpha = 255;
const Color green  = { 50, 255, 100, alpha };
const Color red    = { 255, 50, 50 , alpha };
const Color red2   = { 205, 50, 50 , alpha };
const Color red3   = { 105, 150, 150 , alpha };
const Color blue   = { 50, 100, 255, alpha };
const Color blue2  = { 150, 100, 205, alpha };
const Color yellow = { 155, 255, 50, alpha };
const Color orange = { 255, 100, 50, alpha };
// Color colors[NUMBER_OF_COLORS] = { red, blue, green, yellow, orange };
Color colors[NUMBER_OF_COLORS] = { red, red2, red3, blue, blue2 };


int main(void) {
    visited_points = create_PointSet(NUMBER_OF_STEPS * 2);
    WWIDTH = 1264;
    WHEIGHT = 712;


    // Set seed to current time
    srand(time(NULL));


    Point **p_pts = malloc(sizeof(Point *) * NUMBER_OF_RUNS);
    int *counts = malloc(sizeof(int) * NUMBER_OF_RUNS);
    for (size_t i = 0; i < NUMBER_OF_RUNS; i++) {
        int xi = i / 2;
        int yi = i % 2 + 1;
        // p_pts[i] = random_walk(number_of_steps, -100 + 50 * xi, yi * 50 - 50);
        p_pts[i] = random_walk(&counts[i]);
    }
    size_t iteration_count = 0;
    for (size_t i = 0; i < NUMBER_OF_RUNS; i++) {
        iteration_count += counts[i];
    }

    InitWindow(WWIDTH, WHEIGHT, "Random Walks");
    SetTargetFPS(60);
    
    float step_size = 1.0f;
    float dx = 0.0;
    float dy = 0.0;
    bool drawing_info = true;
    while (!WindowShouldClose()) {
        BeginDrawing();
            Color color = { 51, 51, 51, 255 };
            ClearBackground(color);
            for (size_t k = 0; k < NUMBER_OF_RUNS; k++) {
                Point *pts = p_pts[k];
                for (size_t i = 0; i < counts[k] - 1; i++) {
                    Point p1 = pts[i];
                    Point p2 = pts[i + 1];
                    float x1 = (WWIDTH/2  + step_size * p1.x) + dx;
                    float y1 = (WHEIGHT/2 + step_size * p1.y) + dy;
                    float x2 = (WWIDTH/2  + step_size * p2.x) + dx;
                    float y2 = (WHEIGHT/2 + step_size * p2.y) + dy;
                    // DrawLine(x1, y1, x2, y2, colors[k]);
                    Vector2 startPos = { x1, y1 };
                    Vector2 endPos = { x2, y2 };
                    float thick = 1.2;
                    DrawLineEx(startPos, endPos, thick, colors[k % NUMBER_OF_COLORS]);
                    // DrawCircle(x1, y1, step_size / 4  , colors[k % NUMBER_OF_COLORS]);
                    // DrawCircle(x2, y2, step_size / 4  , colors[k % NUMBER_OF_COLORS]);
                }
            }
        EndDrawing();

        if (IsKeyReleased(KEY_P)) {
            TakeScreenshot(TextFormat("random_walk_%ld.png", GetTime()));
        }

        float zoom_speed = 0.1f;
        if (IsKeyDown(KEY_Z)) {
            step_size += zoom_speed;
        }
        if (IsKeyDown(KEY_X)) {
            step_size -= zoom_speed;
        }
        if (drawing_info) {
            DrawText(TextFormat("Step size: %f", step_size), 10, 10, 20, WHITE);
            DrawText(TextFormat("Iteration count: %ld", iteration_count), 10, 30, 20, WHITE);
        }

        float speed = 2.5f;
        if (IsKeyDown(KEY_A)) {
            dx += speed;
        }
        if (IsKeyDown(KEY_W)) {
            dy += speed;
        }
        if (IsKeyDown(KEY_D)) {
            dx -= speed;
        }
        if (IsKeyDown(KEY_S)) {
            dy -= speed;
        }

        if (IsKeyReleased(KEY_T)) {
            dx = 0.0f;
            dy = 0.0f;
        }
        if (IsKeyReleased(KEY_I)) {
            drawing_info = !drawing_info;
        }
    }

    CloseWindow();

    return 0;
}
